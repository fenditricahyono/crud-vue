<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mahasiswa;

class MahasiswaController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return Mahasiswa::all();
    }

    // mengambil data by id
    public function show($id)
    {
        return Mahasiswa::find($id);
    }

    // menambah data
    public function store(Request $request)
    {
        return Mahasiswa::create($request->all());
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->update($request->all());
        return $mahasiswa;
    }

    // menghapus data
    public function delete($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->delete();
        return 204;
    }
}